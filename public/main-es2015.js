(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<library-header></library-header>\r\n<div class=\"container\" style=\"min-height: calc(100vh - 153px)\">\r\n  <div class=\"row\">\r\n    <div class=\"col\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</div>\r\n<library-footer></library-footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/books/books.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/books/books.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-bordered\">\r\n        <thead>\r\n          <tr>\r\n            <th scope=\"col\" class=\"text-center align-middle\">#</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Category</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">ISBN</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Name</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Author</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Publish date</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Edit</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Delete</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let book of books$ | async; let i = index\">\r\n            <th scope=\"row\" class=\"text-center align-middle\">{{ i + 1 }}</th>\r\n            <td class=\"text-center align-middle\">{{ book.category.name }}</td>\r\n            <td class=\"text-center align-middle\">{{ book.isbn }}</td>\r\n            <td class=\"text-center align-middle\">{{ book.title }}</td>\r\n            <td class=\"text-center align-middle\">{{ book.author }}</td>\r\n            <td class=\"text-center align-middle\">\r\n              {{ book.publishDate | date: 'dd. MMM yyyy.' }}\r\n            </td>\r\n            <td class=\"text-center align-middle\">\r\n              <button\r\n                type=\"button\"\r\n                class=\"btn btn-outline-secondary\"\r\n                (click)=\"onBookEdit(book)\"\r\n                data-toggle=\"modal\"\r\n                data-target=\"#saveBookModal\"\r\n              >\r\n                Edit\r\n              </button>\r\n            </td>\r\n            <td class=\"text-center align-middle\">\r\n              <button\r\n                type=\"button\"\r\n                class=\"btn btn-danger\"\r\n                (click)=\"onBookDelete(book)\"\r\n                data-toggle=\"modal\"\r\n                data-target=\"#deleteBookModal\"\r\n              >\r\n                Delete\r\n              </button>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"float-right\">\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-primary mr-2\"\r\n        data-toggle=\"modal\"\r\n        data-target=\"#saveBookModal\"\r\n        (click)=\"onBookAdd()\"\r\n      >\r\n        Add Book\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Delete Modal -->\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"deleteBookModal\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"deleteBookModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"deleteBookModalLabel\">Delete Book</h5>\r\n        <button\r\n          type=\"button\"\r\n          class=\"close\"\r\n          data-dismiss=\"modal\"\r\n          aria-label=\"Close\"\r\n        >\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>\r\n          Do you want to delete <i>{{ selectedBook?.title }}</i\r\n          >, by <i>{{ selectedBook?.author }}</i\r\n          >?\r\n        </p>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">\r\n          Close\r\n        </button>\r\n        <button\r\n          type=\"button\"\r\n          class=\"btn btn-primary\"\r\n          data-dismiss=\"modal\"\r\n          (click)=\"onBookDeleteSubmit()\"\r\n        >\r\n          Submit\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Save Modal -->\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"saveBookModal\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"saveBookModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"saveBookModalLabel\">Add Book</h5>\r\n        <button\r\n          type=\"button\"\r\n          class=\"close\"\r\n          data-dismiss=\"modal\"\r\n          aria-label=\"Close\"\r\n        >\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <form (ngSubmit)=\"onBookSaveSubmit(f, c)\" #f=\"ngForm\">\r\n        <div class=\"modal-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col\" ngFormGroup>\r\n              <div class=\"form-group\">\r\n                <label for=\"category\">Category</label>\r\n                <select\r\n                  class=\"form-control\"\r\n                  id=\"category\"\r\n                  name=\"category\"\r\n                  [compareWith]=\"compareFn\"\r\n                  [ngModel]=\"selectedBook.category\"\r\n                  required\r\n                  #category\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      category.invalid && (category.dirty || category.touched)\r\n                  }\"\r\n                >\r\n                  <option [ngValue]=\"null\" disabled\r\n                    >Please select category</option\r\n                  >\r\n                  <option\r\n                    *ngFor=\"let category of categories$ | async\"\r\n                    [ngValue]=\"category\"\r\n                    >{{ category.name }}</option\r\n                  >\r\n                </select>\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"\r\n                    category.invalid && (category.dirty || category.touched)\r\n                  \"\r\n                >\r\n                  Please select category!\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"title\">Title</label>\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  id=\"title\"\r\n                  name=\"title\"\r\n                  [(ngModel)]=\"selectedBook.title\"\r\n                  required\r\n                  #title=\"ngModel\"\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      error?.title ||\r\n                      (title.invalid && (title.dirty || title.touched))\r\n                  }\"\r\n                />\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"title.invalid && (title.dirty || title.touched)\"\r\n                >\r\n                  Please enter book title!\r\n                </div>\r\n                <div *ngIf=\"error?.title\" class=\"invalid-feedback\">\r\n                  {{ error.title }}\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"title\">Author</label>\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  id=\"author\"\r\n                  name=\"author\"\r\n                  [(ngModel)]=\"selectedBook.author\"\r\n                  required\r\n                  #author=\"ngModel\"\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      error?.author ||\r\n                      (author.invalid && (author.dirty || author.touched))\r\n                  }\"\r\n                />\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"author.invalid && (author.dirty || author.touched)\"\r\n                >\r\n                  Please enter author name!\r\n                </div>\r\n                <div *ngIf=\"error?.author\" class=\"invalid-feedback\">\r\n                  {{ error.author }}\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"publishDate\">Publish Date</label>\r\n                <input\r\n                  type=\"date\"\r\n                  class=\"form-control\"\r\n                  id=\"publishDate\"\r\n                  name=\"publishDate\"\r\n                  [max]=\"getCurrentDate()\"\r\n                  [ngModel]=\"selectedBook.publishDate | date: 'yyyy-MM-dd'\"\r\n                  (ngModelChange)=\"selectedBook.publishDate = $event\"\r\n                  required\r\n                  #publishDate=\"ngModel\"\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      error?.publishDate ||\r\n                      (publishDate.invalid &&\r\n                        (publishDate.dirty || publishDate.touched))\r\n                  }\"\r\n                />\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"\r\n                    publishDate.invalid &&\r\n                    (publishDate.dirty || publishDate.touched)\r\n                  \"\r\n                >\r\n                  Please select publish date!\r\n                </div>\r\n                <div *ngIf=\"error?.publishDate\" class=\"invalid-feedback\">\r\n                  {{ error.publishDate }}\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"isbn\">ISBN</label>\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  id=\"isbn\"\r\n                  name=\"isbn\"\r\n                  [(ngModel)]=\"selectedBook.isbn\"\r\n                  required\r\n                  #isbn=\"ngModel\"\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      error?.isbn ||\r\n                      (isbn.invalid && (isbn.dirty || isbn.touched))\r\n                  }\"\r\n                />\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"isbn.invalid && (isbn.dirty || isbn.touched)\"\r\n                >\r\n                  Please enter ISBN!\r\n                </div>\r\n                <div *ngIf=\"error?.isbn\" class=\"invalid-feedback\">\r\n                  {{ error.isbn }}\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button\r\n            type=\"button\"\r\n            class=\"btn btn-secondary\"\r\n            data-dismiss=\"modal\"\r\n            #c\r\n          >\r\n            Close\r\n          </button>\r\n          <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!f.valid\">\r\n            Submit\r\n          </button>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/categories/categories.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/categories/categories.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-bordered\">\r\n        <thead>\r\n          <tr>\r\n            <th scope=\"col\" class=\"text-center align-middle\">#</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Category</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Edit</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Delete</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let category of categories$ | async; let i = index\">\r\n            <th scope=\"row\" class=\"text-center align-middle\">{{ i + 1 }}</th>\r\n            <td class=\"text-center align-middle\">{{ category.name }}</td>\r\n            <td class=\"text-center align-middle\">\r\n              <button\r\n                type=\"button\"\r\n                class=\"btn btn-outline-secondary\"\r\n                (click)=\"onCategoryEdit(category)\"\r\n                data-toggle=\"modal\"\r\n                data-target=\"#saveCategoryModal\"\r\n              >\r\n                Edit\r\n              </button>\r\n            </td>\r\n            <td class=\"text-center align-middle\">\r\n              <button\r\n                type=\"button\"\r\n                class=\"btn btn-danger\"\r\n                (click)=\"onCategoryDelete(category)\"\r\n                data-toggle=\"modal\"\r\n                data-target=\"#deleteCategoryModal\"\r\n              >\r\n                Delete\r\n              </button>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"float-right\">\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-primary mr-2\"\r\n        data-toggle=\"modal\"\r\n        data-target=\"#saveCategoryModal\"\r\n        (click)=\"onCategoryAdd()\"\r\n      >\r\n        Add Category\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Delete Modal -->\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"deleteCategoryModal\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"deleteCategoryModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"deleteCategoryModalLabel\">\r\n          Delete Category\r\n        </h5>\r\n        <button\r\n          type=\"button\"\r\n          class=\"close\"\r\n          data-dismiss=\"modal\"\r\n          aria-label=\"Close\"\r\n        >\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>\r\n          Do you want to delete <i>{{ selectedCategory?.name }}</i> and all\r\n          books assigned to this category?\r\n        </p>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">\r\n          Close\r\n        </button>\r\n        <button\r\n          type=\"button\"\r\n          class=\"btn btn-primary\"\r\n          data-dismiss=\"modal\"\r\n          (click)=\"onCategoryDeleteSubmit()\"\r\n        >\r\n          Submit\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- Save Modal -->\r\n<div\r\n  class=\"modal fade\"\r\n  id=\"saveCategoryModal\"\r\n  tabindex=\"-1\"\r\n  role=\"dialog\"\r\n  aria-labelledby=\"saveCategoryModalLabel\"\r\n  aria-hidden=\"true\"\r\n>\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"saveCategoryModalLabel\">\r\n          {{ operation }} Category\r\n        </h5>\r\n        <button\r\n          type=\"button\"\r\n          class=\"close\"\r\n          data-dismiss=\"modal\"\r\n          aria-label=\"Close\"\r\n        >\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <form (ngSubmit)=\"onCategorySaveSubmit(f, c)\" #f=\"ngForm\">\r\n        <div class=\"modal-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"form-group\">\r\n                <label for=\"name\">Name</label>\r\n                <input\r\n                  type=\"text\"\r\n                  class=\"form-control\"\r\n                  id=\"name\"\r\n                  name=\"name\"\r\n                  [(ngModel)]=\"selectedCategory.name\"\r\n                  required\r\n                  #name=\"ngModel\"\r\n                  [ngClass]=\"{\r\n                    'is-invalid':\r\n                      error?.name ||\r\n                      (name.invalid && (name.dirty || name.touched))\r\n                  }\"\r\n                />\r\n                <div\r\n                  class=\"invalid-feedback\"\r\n                  *ngIf=\"name.invalid && (name.dirty || name.touched)\"\r\n                >\r\n                  Please enter category name!\r\n                </div>\r\n                <div *ngIf=\"error?.name\" class=\"invalid-feedback\">\r\n                  {{ error.name }}\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button\r\n            type=\"button\"\r\n            class=\"btn btn-secondary\"\r\n            data-dismiss=\"modal\"\r\n            #c\r\n          >\r\n            Close\r\n          </button>\r\n          <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!f.valid\">\r\n            Submit\r\n          </button>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/contact/contact.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/contact/contact.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Save Modal -->\r\n<form (ngSubmit)=\"onContactSubmit(f, c)\" #f=\"ngForm\">\r\n  <div class=\"modal-body\">\r\n    <div class=\"row\">\r\n      <div class=\"col\">\r\n        <div class=\"form-group\">\r\n          <label for=\"email\">Email</label>\r\n          <input\r\n            [email]=\"true\"\r\n            libraryMyValidationDirective\r\n            type=\"text\"\r\n            class=\"form-control\"\r\n            id=\"email\"\r\n            name=\"email\"\r\n            required\r\n            #email=\"ngModel\"\r\n            ngModel\r\n            [ngClass]=\"{\r\n              'is-invalid':\r\n                error?.name || (email.invalid && (email.dirty || email.touched))\r\n            }\"\r\n          />\r\n          <div\r\n            class=\"invalid-feedback\"\r\n            *ngIf=\"email.invalid && (email.dirty || email.touched)\"\r\n          >\r\n            Please enter correct email!\r\n          </div>\r\n          <div *ngIf=\"error?.name\" class=\"invalid-feedback\">\r\n            {{ error.name }}\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col\">\r\n            <div class=\"form-group\">\r\n              <label for=\"message\">Message</label>\r\n              <input\r\n                type=\"text\"\r\n                class=\"form-control\"\r\n                id=\"message\"\r\n                name=\"message\"\r\n                ngModel\r\n                required\r\n                #message=\"ngModel\"\r\n                [ngClass]=\"{\r\n                  'is-invalid':\r\n                    error?.name ||\r\n                    (message.invalid && (message.dirty || message.touched))\r\n                }\"\r\n              />\r\n              <div\r\n                class=\"invalid-feedback\"\r\n                *ngIf=\"message.invalid && (message.dirty || message.touched)\"\r\n              >\r\n                Please enter correct message!\r\n              </div>\r\n              <div *ngIf=\"error?.name\" class=\"invalid-feedback\">\r\n                {{ error.name }}\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button\r\n      type=\"button\"\r\n      class=\"btn btn-secondary\"\r\n      (click)=\"onContactCancel(f)\"\r\n      #c\r\n    >\r\n      Close\r\n    </button>\r\n    <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!f.valid\">\r\n      Submit\r\n    </button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/footer/footer.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/footer/footer.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\r\n    <div class=\"card-footer text-muted text-center\">\r\n        Levi9 &copy; 2019\r\n    </div>\r\n  </footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/header/header.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/header/header.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light mb-5\">\r\n  <div class=\"container\">\r\n    <a class=\"navbar-brand\" routerLink=\"/\">Library</a>\r\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarNavDropdown\">\r\n      <ul class=\"navbar-nav\">\r\n        <ng-template [ngIf]=\"authService.isAuthenticated()\">\r\n          <li class=\"nav-item\" routerLinkActive=\"active\">\r\n            <a class=\"nav-link\" routerLink=\"/home\">Home</a>\r\n          </li>\r\n        </ng-template>        \r\n        <ng-template [ngIf]=\"authService.hasRoleAdmin()\">                    \r\n          <li class=\"nav-item\" routerLinkActive=\"active\">\r\n            <a class=\"nav-link\" routerLink=\"/categories\">Categories</a>\r\n          </li>\r\n          <li class=\"nav-item\" routerLinkActive=\"active\">\r\n            <a class=\"nav-link\" routerLink=\"/books\">Books</a>\r\n          </li>\r\n        </ng-template>\r\n      </ul>\r\n      <ul class=\"navbar-nav ml-auto\">\r\n        <ng-template [ngIf]=\"!authService.isAuthenticated()\">\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/registration\">Register</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" routerLink=\"/login\">Login</a>\r\n          </li>\r\n        </ng-template>\r\n        <ng-template [ngIf]=\"authService.isAuthenticated()\">\r\n            <li class=\"nav-item dropdown\">\r\n              <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                {{ authService.getUsername() }}\r\n              </a>\r\n              <div class=\"dropdown-menu  dropdown-menu-right\" aria-labelledby=\"navbarDropdownMenuLink\">\r\n                <a class=\"dropdown-item\" (click)=\"authService.logout()\" routerLink=\"/\">Logout</a>\r\n              </div>\r\n            </li>\r\n          </ng-template>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/book-list/book-list.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/book-list/book-list.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"table-responsive\">\r\n      <table class=\"table table-bordered\">\r\n        <thead>\r\n          <tr>\r\n            <th scope=\"col\" class=\"text-center align-middle\">#</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Name</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Image</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Author</th>\r\n            <th scope=\"col\" class=\"text-center align-middle\">Publish date</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr\r\n            *ngFor=\"\r\n              let book of books$\r\n                | async\r\n                | filterBooksByCategory: selectedCategoryId;\r\n              let i = index\r\n            \"\r\n          >\r\n            <th scope=\"row\" class=\"text-center align-middle\">{{ i + 1 }}</th>\r\n            <td class=\"text-center align-middle\">{{ book.title }}</td>\r\n            <td class=\"text-center align-middle\">\r\n              <img\r\n                src=\"assets/images/placeholder.jpg\"\r\n                alt=\"placeholder\"\r\n                class=\"img-thumbnail\"\r\n              />\r\n            </td>\r\n            <td class=\"text-center align-middle\">{{ book.author }}</td>\r\n            <td class=\"text-center align-middle\">\r\n              {{ book.publishDate | date: 'dd. MMM yyyy.' }}\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/category-list/category-list.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/category-list/category-list.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"list-group mb-4 mt-3 mt-lg-0\">\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-outline-secondary list-group-item\"\r\n        [ngClass]=\"{ active: selectedCategoryId === null }\"\r\n        (click)=\"onCategorySelect(null)\"\r\n      >\r\n        All\r\n      </button>\r\n      <button\r\n        *ngFor=\"let category of categories$ | async\"\r\n        type=\"button\"\r\n        [ngClass]=\"{ active: selectedCategoryId === category._id }\"\r\n        class=\"btn btn-outline-secondary list-group-item\"\r\n        (click)=\"onCategorySelect(category._id)\"\r\n      >\r\n        {{ category.name }}\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col\">\r\n    <div class=\"jumbotron\">\r\n      <h1 class=\"display-5\">Library</h1>\r\n      <p class=\"lead\">\r\n        Library is simple web application design to represent virtual library.\r\n        Project was intentionally kept simple ie. it has only few entities but\r\n        it shows usage of modern web technologies.\r\n      </p>\r\n      <hr class=\"my-4\" />\r\n      <p>\r\n        Project will be organized in several sections (named steps) where each\r\n        one represents the final stage of the finished section.\r\n      </p>\r\n      <p class=\"lead\">\r\n        <a\r\n          class=\"btn btn-primary btn-lg\"\r\n          href=\"https://bitbucket.org/a_madjarev/library/src/master/\"\r\n          role=\"button\"\r\n          >Learn more</a\r\n        >\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-9\">\r\n    <library-book-list></library-book-list>\r\n  </div>\r\n  <div class=\"col-lg-3\">\r\n    <library-category-list\r\n     \r\n    ></library-category-list>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row justify-content-center\">\r\n    <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4\">\r\n      <form (ngSubmit)=\"onLogin(f)\" #f=\"ngForm\">\r\n        <div class=\"form-group\">\r\n          <label for=\"email\">E-Mail</label>\r\n          <input class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" ngModel required email #email=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.email || (email.invalid && (email.dirty || email.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"email.invalid && (email.dirty || email.touched)\">\r\n            Please enter correct email!\r\n          </div>\r\n          <div *ngIf=\"error?.email\" class=\"invalid-feedback\">{{error.email}}</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"password\">Password</label>\r\n          <input class=\"form-control\" type=\"password\" id=\"password\" name=\"password\" ngModel required #password=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.password || (password.invalid && (password.dirty || password.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"password.invalid && (password.dirty || password.touched)\">\r\n            Please enter password!\r\n          </div>\r\n          <div *ngIf=\"error?.password\" class=\"invalid-feedback\">{{error.password}}</div>\r\n        </div>\r\n        <div class=\"alert alert-danger\" *ngIf=\"error\">\r\n          Wrong email or/and password!\r\n        </div>\r\n        <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Login</button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/not-found-page/not-found-page.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/not-found-page/not-found-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>not-found-page works!</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/registration/registration.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/registration/registration.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row justify-content-center\">\r\n    <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4\">\r\n      <form (ngSubmit)=\"onRegister(f)\" #f=\"ngForm\">\r\n        <div class=\"form-group\">\r\n          <label for=\"name\">Name</label>\r\n          <input class=\"form-control\" type=\"text\" id=\"name\" name=\"name\" ngModel required #name=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.name || (name.invalid && (name.dirty || name.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"name.invalid && (name.dirty || name.touched)\">\r\n            Please enter your name!\r\n          </div>\r\n          <div *ngIf=\"error?.name\" class=\"invalid-feedback\">{{error.name}}</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"email\">E-Mail</label>\r\n          <input class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" ngModel required email #email=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.email || (email.invalid && (email.dirty || email.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"email.invalid && (email.dirty || email.touched)\">\r\n            Please enter correct email!\r\n          </div>\r\n          <div *ngIf=\"error?.email\" class=\"invalid-feedback\">{{error.email}}</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"password\">Password</label>\r\n          <input class=\"form-control\" type=\"password\" id=\"password\" name=\"password\" ngModel required minlength=\"6\" #password=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.password || (password.invalid && (password.dirty || password.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"password.invalid && (password.dirty || password.touched)\">\r\n            Please enter password with at least 6 characters!\r\n          </div>\r\n          <div *ngIf=\"error?.password\" class=\"invalid-feedback\">{{error.password}}</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"password2\">Confirm Password</label>\r\n          <input class=\"form-control\" type=\"password\" id=\"password2\" name=\"password2\" ngModel required #password2=\"ngModel\"\r\n            [ngClass]=\"{'is-invalid': error?.password2 || (password2.invalid && (password2.dirty || password2.touched))}\">\r\n          <div class=\"invalid-feedback\" *ngIf=\"password2.invalid && (password2.dirty || password2.touched)\">\r\n            Please enter conformation password!\r\n          </div>\r\n          <div *ngIf=\"error?.password2\" class=\"invalid-feedback\">{{error.password2}}</div>\r\n        </div>\r\n        <div class=\"alert alert-danger\" *ngIf=\"error\">\r\n          Failed to register user!\r\n        </div>\r\n        <div class=\"alert alert-success\" *ngIf=\"success\">\r\n          You have successfully registered!\r\n        </div>\r\n        <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Register</button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _categories_categories_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./categories/categories.component */ "./src/app/categories/categories.component.ts");
/* harmony import */ var _books_books_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./books/books.component */ "./src/app/books/books.component.ts");
/* harmony import */ var _not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./not-found-page/not-found-page.component */ "./src/app/not-found-page/not-found-page.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");











const routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    {
        path: 'home',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
        canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]],
        data: { expectedRole: 'user' }
    },
    {
        path: 'categories',
        component: _categories_categories_component__WEBPACK_IMPORTED_MODULE_4__["CategoriesComponent"],
        canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]],
        data: { expectedRole: 'admin' }
    },
    {
        path: 'books',
        component: _books_books_component__WEBPACK_IMPORTED_MODULE_5__["BooksComponent"],
        canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]],
        data: { expectedRole: 'admin' }
    },
    { path: 'contacts', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_7__["ContactComponent"] },
    { path: 'registration', component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_8__["RegistrationComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"] },
    { path: '**', component: _not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_6__["NotFoundPageComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");



let AppComponent = class AppComponent {
    constructor(authService) {
        this.authService = authService;
        this.title = 'ng-library';
    }
    ngOnInit() {
        this.authService.autoLogin();
    }
};
AppComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _books_books_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./books/books.component */ "./src/app/books/books.component.ts");
/* harmony import */ var _categories_categories_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./categories/categories.component */ "./src/app/categories/categories.component.ts");
/* harmony import */ var _home_category_list_category_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home/category-list/category-list.component */ "./src/app/home/category-list/category-list.component.ts");
/* harmony import */ var _home_book_list_book_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home/book-list/book-list.component */ "./src/app/home/book-list/book-list.component.ts");
/* harmony import */ var _filter_books_by_category_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./filter-books-by-category.pipe */ "./src/app/filter-books-by-category.pipe.ts");
/* harmony import */ var _not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./not-found-page/not-found-page.component */ "./src/app/not-found-page/not-found-page.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _contact_email_form_validator_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./contact-email-form-validator.directive */ "./src/app/contact-email-form-validator.directive.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");





















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
            _header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
            _books_books_component__WEBPACK_IMPORTED_MODULE_10__["BooksComponent"],
            _categories_categories_component__WEBPACK_IMPORTED_MODULE_11__["CategoriesComponent"],
            _home_category_list_category_list_component__WEBPACK_IMPORTED_MODULE_12__["CategoryListComponent"],
            _home_book_list_book_list_component__WEBPACK_IMPORTED_MODULE_13__["BookListComponent"],
            _filter_books_by_category_pipe__WEBPACK_IMPORTED_MODULE_14__["FilterBooksByCategoryPipe"],
            _not_found_page_not_found_page_component__WEBPACK_IMPORTED_MODULE_15__["NotFoundPageComponent"],
            _contact_contact_component__WEBPACK_IMPORTED_MODULE_16__["ContactComponent"],
            _contact_email_form_validator_directive__WEBPACK_IMPORTED_MODULE_17__["ContactEmailFormValidatorDirective"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_18__["RegistrationComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_20__["LoginComponent"]
        ],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]],
        providers: [_auth_service__WEBPACK_IMPORTED_MODULE_19__["AuthService"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");






let AuthGuard = 
/*
  next ruta na koju idemo
  state je ruta sa koje idemo

*/
class AuthGuard {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    canActivate(next, state) {
        const expectedRole = next.data.expectedRole;
        return this.authService.getCurrentUser().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(user => {
            if (expectedRole === 'user' ||
                (expectedRole === 'admin' && user.role === 4)) {
                return true; // da moze da ode na rutu
            }
            return this.router.createUrlTree(['/home']);
            // da izgradis URL rutu na koju ces da ga posaljes ---> moze da se iskoristi da ga vratis ako nije Auth
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(() => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.router.createUrlTree(['/login']));
        }));
    }
};
AuthGuard.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])({
        providedIn: 'root'
    })
    /*
      next ruta na koju idemo
      state je ruta sa koje idemo
    
    */
    ,
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let AuthService = class AuthService {
    constructor(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
        //API = 'http://localhost:5000/api/users';
        this.API = 'api/users';
        this.authenticated = false;
    }
    register(name, email, password, password2) {
        return this.httpClient.post(this.API + '/register', {
            name,
            email,
            password,
            password2
        });
    }
    // pipe --> unutar nje se uvezuju svi ostALI OPERATORI NAD OBSERVABLOM KOJI JE STIGAO SA BackEnd-a --> redjaju se u njemu sa ZAREZOM
    // tap  --> ne utica na krajnji obsevable, napravi KOPIJU obsevabla
    login(email, password) {
        return this.httpClient
            .post(this.API + '/login', { email, password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(resData => {
            localStorage.setItem('token', resData.token); // upises ga u localStorage
            this.setAuthHeaders(resData.token);
            this.getCurrentUser().subscribe(user => {
                this.user = user;
                this.authenticated = true;
                this.router.navigate(['/home']);
            });
        }));
    }
    getCurrentUser() {
        return this.httpClient.get(this.API + '/current', {
            headers: this.headers
        });
    }
    isAuthenticated() {
        return this.authenticated;
    }
    getAuthHeaders() {
        return this.headers;
    }
    setAuthHeaders(token) {
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            Authorization: token
        });
    }
    getUsername() {
        if (this.user) {
            return this.user.name;
        }
    }
    hasRoleAdmin() {
        if (this.user) {
            return this.user.role === 4;
        }
    }
    logout() {
        this.authenticated = false;
        this.user = null;
        this.headers = null;
        this.router.navigate(['/login']);
        localStorage.removeItem('token');
    }
    autoLogin() {
        const token = localStorage.getItem('token');
        if (!token) {
            return;
        }
        this.setAuthHeaders(token);
        this.getCurrentUser().subscribe(user => {
            this.user = user;
            this.authenticated = true;
        });
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AuthService);



/***/ }),

/***/ "./src/app/books/book.model.ts":
/*!*************************************!*\
  !*** ./src/app/books/book.model.ts ***!
  \*************************************/
/*! exports provided: Book */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Book", function() { return Book; });
/* harmony import */ var _categories_category_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../categories/category.model */ "./src/app/categories/category.model.ts");

class Book {
    constructor(id, isbn, category, title, author, publishDate) {
        this._id = id;
        this.isbn = isbn;
        this.category = category;
        this.title = title;
        this.author = author;
        this.publishDate = publishDate;
    }
}
Book.ctorParameters = () => [
    { type: Number },
    { type: String },
    { type: _categories_category_model__WEBPACK_IMPORTED_MODULE_0__["Category"] },
    { type: String },
    { type: String },
    { type: Date }
];


/***/ }),

/***/ "./src/app/books/book.service.ts":
/*!***************************************!*\
  !*** ./src/app/books/book.service.ts ***!
  \***************************************/
/*! exports provided: BookService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookService", function() { return BookService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");




// providedIn root znaci da ce nam instanca biti kreirana za root koponentu i svaka komponenta koja je child od root ce koristiti istu
// taj servis kao u Spring-u
// observalbe je genericka klasa
// rxjs sadrzi observable
// u app.modules mozemo da stavimo u providers dodajemo nas servis ---> OVO je MORALO PRE
// mozes i da stavis u svakom @NgModules --> provides:[naziv_servisa] ---> ta kompoenta i sve komponente koje su child ove bi imale isti servis
// --> razliciti @NgModules ce imati razlicite servise injektovane
let BookService = class BookService {
    constructor(httpClient, authService) {
        this.httpClient = httpClient;
        this.authService = authService;
        //API = "http://localhost:5000/api/books";
        this.API = 'api/books'; // deploy API
    }
    getBooks() {
        return this.httpClient.get(this.API, {
            headers: this.authService.getAuthHeaders()
        });
    }
    saveBook(book) {
        return this.httpClient.post(this.API, book, {
            headers: this.authService.getAuthHeaders()
        });
    }
    deleteBook(bookId) {
        return this.httpClient.delete(this.API + '/' + bookId, {
            headers: this.authService.getAuthHeaders()
        });
    }
};
BookService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
BookService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
        _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
], BookService);



/***/ }),

/***/ "./src/app/books/books.component.scss":
/*!********************************************!*\
  !*** ./src/app/books/books.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Jvb2tzL2Jvb2tzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/books/books.component.ts":
/*!******************************************!*\
  !*** ./src/app/books/books.component.ts ***!
  \******************************************/
/*! exports provided: BooksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksComponent", function() { return BooksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _book_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./book.model */ "./src/app/books/book.model.ts");
/* harmony import */ var _book_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./book.service */ "./src/app/books/book.service.ts");
/* harmony import */ var _categories_category_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../categories/category.service */ "./src/app/categories/category.service.ts");






let BooksComponent = class BooksComponent {
    constructor(bookService, categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
        this.selectedBook = new _book_model__WEBPACK_IMPORTED_MODULE_3__["Book"](null, null, null, null, null, null);
    }
    ngOnInit() {
        this.books$ = this.bookService.getBooks();
        this.categories$ = this.categoryService.getCategories();
    }
    onBookDelete(book) {
        this.selectedBook = Object.assign({}, book);
    }
    onBookDeleteSubmit() {
        this.bookService.deleteBook(this.selectedBook._id).subscribe(() => {
            this.books$ = this.bookService.getBooks();
            this.selectedBook = new _book_model__WEBPACK_IMPORTED_MODULE_3__["Book"](null, null, null, null, null, null);
        }, error => console.error(error));
    }
    // kad resetujes formu mozes da prosledis i objekat sa defaultnim vrednostima
    onBookAdd() {
        this.operation = 'Add';
        this.saveBookForm.reset({ publishDate: this.getCurrentDate() });
        this.selectedBook = new _book_model__WEBPACK_IMPORTED_MODULE_3__["Book"](null, null, null, null, null, new Date(this.getCurrentDate()));
        this.error = null;
    }
    onBookEdit(book) {
        this.operation = 'Edit';
        // pravi duboku kopiju da ne bi izmenio na nevalidno ovo nema kod kategorija........
        this.selectedBook = JSON.parse(JSON.stringify(book));
        this.error = { title: null, isbn: null, author: null, publishDate: null };
    }
    onBookSaveSubmit(form, closeButton) {
        const book = new _book_model__WEBPACK_IMPORTED_MODULE_3__["Book"](this.operation === 'Add' ? null : this.selectedBook._id, form.value.isbn, form.value.category, form.value.title, form.value.author, form.value.publishDate);
        this.bookService.saveBook(book).subscribe(() => {
            this.books$ = this.bookService.getBooks();
            closeButton.click();
        }, (httpErrorResponse) => {
            this.error = httpErrorResponse.error;
        });
    }
    getCurrentDate() {
        return new Date().toISOString().slice(0, 10);
    }
    compareFn(c1, c2) {
        return c1 && c2 ? c1._id === c2._id : c1 === c2;
    }
};
BooksComponent.ctorParameters = () => [
    { type: _book_service__WEBPACK_IMPORTED_MODULE_4__["BookService"] },
    { type: _categories_category_service__WEBPACK_IMPORTED_MODULE_5__["CategoryService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
], BooksComponent.prototype, "saveBookForm", void 0);
BooksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-books',
        template: __webpack_require__(/*! raw-loader!./books.component.html */ "./node_modules/raw-loader/index.js!./src/app/books/books.component.html"),
        styles: [__webpack_require__(/*! ./books.component.scss */ "./src/app/books/books.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_book_service__WEBPACK_IMPORTED_MODULE_4__["BookService"],
        _categories_category_service__WEBPACK_IMPORTED_MODULE_5__["CategoryService"]])
], BooksComponent);



/***/ }),

/***/ "./src/app/categories/categories.component.scss":
/*!******************************************************!*\
  !*** ./src/app/categories/categories.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhdGVnb3JpZXMvY2F0ZWdvcmllcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/categories/categories.component.ts":
/*!****************************************************!*\
  !*** ./src/app/categories/categories.component.ts ***!
  \****************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./category.service */ "./src/app/categories/category.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");




let CategoriesComponent = class CategoriesComponent {
    constructor(categoryService) {
        this.categoryService = categoryService;
        // mozes da prosledis i objekat ovako bez konstruktora za neke jednostavne stvari
        this.selectedCategory = { _id: null, name: null };
    }
    ngOnInit() {
        this.categories$ = this.categoryService.getCategories();
    }
    onCategoryEdit(category) {
        this.operation = 'Edit';
        this.selectedCategory = Object.assign({}, category);
        this.error = null;
    }
    onCategoryDelete(category) {
        this.selectedCategory = Object.assign({}, category);
    }
    onCategoryDeleteSubmit() {
        this.categoryService.deleteCategory(this.selectedCategory._id).subscribe(() => {
            this.categories$ = this.categoryService.getCategories();
            this.selectedCategory = { _id: null, name: null };
        }, error => console.error(error));
    }
    onCategoryAdd() {
        this.operation = 'Add';
        this.addCategoryForm.reset();
        this.selectedCategory = { _id: null, name: null };
        this.error = null;
    }
    onCategorySaveSubmit(form, closeButton) {
        this.categoryService
            .saveCategory({
            _id: this.operation === 'Add' ? null : this.selectedCategory._id,
            name: form.value.name
        })
            .subscribe(() => {
            this.categories$ = this.categoryService.getCategories();
            closeButton.click(); // ovako ces da zatvoris modalni dijalog, ako je sve okej
        }, (httpErrorResponse) => {
            this.error = httpErrorResponse.error;
        });
    }
};
CategoriesComponent.ctorParameters = () => [
    { type: _category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
], CategoriesComponent.prototype, "addCategoryForm", void 0);
CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-categories',
        template: __webpack_require__(/*! raw-loader!./categories.component.html */ "./node_modules/raw-loader/index.js!./src/app/categories/categories.component.html"),
        styles: [__webpack_require__(/*! ./categories.component.scss */ "./src/app/categories/categories.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"]])
], CategoriesComponent);



/***/ }),

/***/ "./src/app/categories/category.model.ts":
/*!**********************************************!*\
  !*** ./src/app/categories/category.model.ts ***!
  \**********************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
class Category {
    constructor(id, name) {
        this._id = id;
        this.name = name;
    }
}
Category.ctorParameters = () => [
    { type: Number },
    { type: String }
];


/***/ }),

/***/ "./src/app/categories/category.service.ts":
/*!************************************************!*\
  !*** ./src/app/categories/category.service.ts ***!
  \************************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");





let CategoryService = class CategoryService {
    constructor(httpClient, authService) {
        this.httpClient = httpClient;
        this.authService = authService;
        //API = 'http://localhost:5000/api/categories';
        this.API = 'api/categories'; // deploy API
        this.categoryFilterChanged = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"](); // mi cemo da emitujemo nase vrednosti pomocu observable, subject-a
    }
    // Return Observable that wraps array of Categories
    getCategories() {
        return this.httpClient.get(this.API, {
            headers: this.authService.getAuthHeaders()
        });
    }
    // Update category if book already has an ID, save it otherwise and return Observable
    saveCategory(category) {
        return this.httpClient.post(this.API, category, {
            headers: this.authService.getAuthHeaders()
        });
    }
    // Delete category by ID and return Observable
    deleteCategory(categoryId) {
        return this.httpClient.delete(this.API + '/' + categoryId, {
            headers: this.authService.getAuthHeaders()
        });
    }
};
CategoryService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
CategoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
        _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
], CategoryService);



/***/ }),

/***/ "./src/app/contact-email-form-validator.directive.ts":
/*!***********************************************************!*\
  !*** ./src/app/contact-email-form-validator.directive.ts ***!
  \***********************************************************/
/*! exports provided: ContactEmailFormValidatorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactEmailFormValidatorDirective", function() { return ContactEmailFormValidatorDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");

var ContactEmailFormValidatorDirective_1;


let ContactEmailFormValidatorDirective = ContactEmailFormValidatorDirective_1 = class ContactEmailFormValidatorDirective {
    constructor() { }
    validate(control) {
        return control.value ? this.emailValidatorFunction(control.value) : null;
    }
    emailValidatorFunction(email) {
        console.log(email);
        const isValid = email.includes('@');
        console.log('proveravam');
        return !isValid ? { identityRevealed: true } : null;
    }
};
ContactEmailFormValidatorDirective = ContactEmailFormValidatorDirective_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[libraryMyValidationDirective]',
        providers: [
            {
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
                useExisting: ContactEmailFormValidatorDirective_1,
                multi: true
            }
        ]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContactEmailFormValidatorDirective);



/***/ }),

/***/ "./src/app/contact/contact.component.scss":
/*!************************************************!*\
  !*** ./src/app/contact/contact.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ContactComponent = class ContactComponent {
    constructor() { }
    ngOnInit() { }
    onContactSubmit(form, closeButton) {
        console.log('email: ' + form.value.email + ' message: ' + form.value.message);
    }
    onContactCancel(form) {
        form.reset();
        console.log('pobrisano iz forme');
    }
};
ContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-contact',
        template: __webpack_require__(/*! raw-loader!./contact.component.html */ "./node_modules/raw-loader/index.js!./src/app/contact/contact.component.html"),
        styles: [__webpack_require__(/*! ./contact.component.scss */ "./src/app/contact/contact.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContactComponent);



/***/ }),

/***/ "./src/app/filter-books-by-category.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/filter-books-by-category.pipe.ts ***!
  \**************************************************/
/*! exports provided: FilterBooksByCategoryPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterBooksByCategoryPipe", function() { return FilterBooksByCategoryPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FilterBooksByCategoryPipe = class FilterBooksByCategoryPipe {
    transform(books, categoryId) {
        if (categoryId) {
            return books.filter(book => book.category._id === categoryId);
        }
        return books;
    }
};
FilterBooksByCategoryPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filterBooksByCategory'
    })
], FilterBooksByCategoryPipe);



/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-footer',
        template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FooterComponent);



/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");



let HeaderComponent = class HeaderComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-header',
        template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
], HeaderComponent);



/***/ }),

/***/ "./src/app/home/book-list/book-list.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/home/book-list/book-list.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvYm9vay1saXN0L2Jvb2stbGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/home/book-list/book-list.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/book-list/book-list.component.ts ***!
  \*******************************************************/
/*! exports provided: BookListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookListComponent", function() { return BookListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_books_book_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/books/book.service */ "./src/app/books/book.service.ts");
/* harmony import */ var src_app_categories_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/categories/category.service */ "./src/app/categories/category.service.ts");




let BookListComponent = class BookListComponent {
    constructor(bookService, categoryService) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }
    ngOnInit() {
        this.books$ = this.bookService.getBooks();
        this.categoryFilterSubscription = this.categoryService.categoryFilterChanged.subscribe(selectedCategoryId => {
            this.selectedCategoryId = selectedCategoryId;
        });
    }
    // da ne bi doslo do memory leak-a ---> kad ubijas komponentu na sve sto je pretplacena TREBA DA se otkaci
    ngOnDestroy() {
        this.categoryFilterSubscription.unsubscribe();
    }
};
BookListComponent.ctorParameters = () => [
    { type: src_app_books_book_service__WEBPACK_IMPORTED_MODULE_2__["BookService"] },
    { type: src_app_categories_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"] }
];
BookListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-book-list',
        template: __webpack_require__(/*! raw-loader!./book-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/book-list/book-list.component.html"),
        styles: [__webpack_require__(/*! ./book-list.component.scss */ "./src/app/home/book-list/book-list.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_books_book_service__WEBPACK_IMPORTED_MODULE_2__["BookService"],
        src_app_categories_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"]])
], BookListComponent);



/***/ }),

/***/ "./src/app/home/category-list/category-list.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/home/category-list/category-list.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvY2F0ZWdvcnktbGlzdC9jYXRlZ29yeS1saXN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/home/category-list/category-list.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/home/category-list/category-list.component.ts ***!
  \***************************************************************/
/*! exports provided: CategoryListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryListComponent", function() { return CategoryListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _categories_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../categories/category.service */ "./src/app/categories/category.service.ts");



let CategoryListComponent = class CategoryListComponent {
    // ovako radi inject servisa --> DI preko konstruktora.. ti kazes da ti treba a on ce ga posle sam dodati
    constructor(categorySevice) {
        this.categorySevice = categorySevice;
        // objekat emitera je ovo sa dekoratorom output --> ovo je kao event u Vue. sad imas
        this.categorySelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // KORISCENO ZA 1. nacin
        this.selectedCategoryId = null;
    }
    ngOnInit() {
        this.categories$ = this.categorySevice.getCategories();
        // subscribe ima 3 fije ovo sto smo mi dali je uspesna, treba call back i za error i za complete
        // this.categories$.subscribe(
        //   data => {
        //     console.log(data);
        //   },
        //   error => {
        //     console.error(error);
        //   },
        //   () => {
        //     console.log('complete');
        //   }
        // );
    }
    onCategorySelect(categoryId) {
        this.selectedCategoryId = categoryId;
        // ovo je bilo na prvi nacin
        //this.categorySelected.emit(categoryId);
        // drugi nacin sa servisima
        this.categorySevice.categoryFilterChanged.next(categoryId);
    }
};
CategoryListComponent.ctorParameters = () => [
    { type: _categories_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], CategoryListComponent.prototype, "categorySelected", void 0);
CategoryListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-category-list',
        template: __webpack_require__(/*! raw-loader!./category-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/category-list/category-list.component.html"),
        styles: [__webpack_require__(/*! ./category-list.component.scss */ "./src/app/home/category-list/category-list.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_categories_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"]])
], CategoryListComponent);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    // koristi se u prvom nacinu kad se komunicira preko parenta
    //selectedCategoryID: number;
    // brise se props iz .html fajla koji je prosledjivao ovo na ono @Input u category-list.component.ts
    constructor() { }
    ngOnInit() { }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");



let LoginComponent = class LoginComponent {
    constructor(authService) {
        this.authService = authService;
        this.error = null;
    }
    ngOnInit() {
    }
    onLogin(form) {
        const email = form.value.email;
        const password = form.value.password;
        this.authService.login(email, password)
            .subscribe((res) => {
            this.error = null;
        }, (httpErrorResponse) => {
            this.error = httpErrorResponse.error;
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/not-found-page/not-found-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/not-found-page/not-found-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vdC1mb3VuZC1wYWdlL25vdC1mb3VuZC1wYWdlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/not-found-page/not-found-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/not-found-page/not-found-page.component.ts ***!
  \************************************************************/
/*! exports provided: NotFoundPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundPageComponent", function() { return NotFoundPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NotFoundPageComponent = class NotFoundPageComponent {
    constructor() { }
    ngOnInit() { }
};
NotFoundPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-not-found-page',
        template: __webpack_require__(/*! raw-loader!./not-found-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/not-found-page/not-found-page.component.html"),
        styles: [__webpack_require__(/*! ./not-found-page.component.scss */ "./src/app/not-found-page/not-found-page.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NotFoundPageComponent);



/***/ }),

/***/ "./src/app/registration/registration.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/registration/registration.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/registration/registration.component.ts":
/*!********************************************************!*\
  !*** ./src/app/registration/registration.component.ts ***!
  \********************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../auth.service */ "./src/app/auth.service.ts");



let RegistrationComponent = class RegistrationComponent {
    constructor(authService) {
        this.authService = authService;
        this.error = null;
        this.success = false;
    }
    ngOnInit() {
    }
    onRegister(form) {
        const name = form.value.name;
        const email = form.value.email;
        const password = form.value.password;
        const password2 = form.value.password2;
        this.authService.register(name, email, password, password2)
            .subscribe(() => {
            this.error = null;
            this.success = true;
        }, (httpErrorResponse) => {
            this.error = httpErrorResponse.error;
            this.success = false;
        });
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'library-registration',
        template: __webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/registration/registration.component.html"),
        styles: [__webpack_require__(/*! ./registration.component.scss */ "./src/app/registration/registration.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
], RegistrationComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\dusan\OneDrive\Desktop\Levi9SummerWorkShop\predavanjaUcenje\mylibrary\ng-library\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map