const ObjectId = require("mongoose").Types.ObjectId;
const ISBN = require("is-isbn");

// Load Validation
const vallidateBookInput = require("../validation/book");

// Load models
const Book = require("../models/Book");

exports.getAllBooks = function(req, res){
            let errors = {};
            Book.find()
            .populate("category", ["name"])
            .then(books => {
                if (books.length === 0) {
                errors.no_books = "There are no books";
                return res.status(400).json(errors);
                }
        
                return res.status(200).json(books);
            })
            .catch(err => {
                return res.status(404).json({
                msg: "There was an error fetching books from database",
                err: err
                });
            });
        }


