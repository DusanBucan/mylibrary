const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

const users = require('./routes/user');
const categories = require('./routes/categories');
const books = require('./routes/book');

const app = express();

// CORS filter
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  next();
});

const dbString = require('./config/keys').mongoURI; // lagano pokupio promenljivu
const db = process.env.MONGODB_URI || dbString;

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false })); // for parsing application/x-www-form-urlencoded
// ovo znaci---> body objekat koji stigne sa fronta u ZAHTEVU
// pretvara u JSON objekat
app.use(bodyParser.json()); //for parsing application/json

/*
  posto require u svakom fajlu ce ucitavati 
  istu instacu mongoose-a ovde kad mu podesimo
  konekciju u svim fajlovima ta konekcija ce
  biti vidljiva??????
*/

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true
  })
  .then(() => {
    console.log('MongoDB Connected');
  })
  .catch(err => {
    console.log(err);
  });

/* Use routes --> ovo je samo da ne mora
  u svakom kontroleru da kucas ceo prefiks nego 
  samo onaj deo putanje ---> ovo ovde navedeno
  se nalepi na onu putanju za --> meni za users izgleda 
  ovako:

    /api/users/users --> ovde 2. users je iz kontrolera
*/

// Passport middleware
app.use(passport.initialize()); // onaj middleawre od gore ucitan

// Passport Config
require('./config/passport')(passport); // ovo smo fiju ucitali i sad njoj kad prosledimo middleware ona doda Strategiju u Passport middleware

app.use('/api/users', users);
app.use('/api/categories', categories);
app.use('/api/books', books);

//app.get('/', (reg, res) => res.send('Invalid Endpoint'));

// DA ANGULAR APP BUDE DOSTUPNA KAD DEPLOY ---> ovo je kad je izbildas i skines ono http:localhost/3000/
app.use(express.static(path.join(__dirname, 'public')));
app.get('*', (reg, res) =>
  res.sendFile(path.join(__dirname, 'public/index.html'))
);

const port = process.env.PORT || 5000; // ovo process.env.PORT ce biti BITNO KOD DEPLOYA!!!!!

app.listen(port, () => console.log(`server Server running on port ${port}`));

module.exports = app; // ovo ce trebati kad budemo pisali testove
