const chai = require("chai");
const chaiHttp = require("chai-http");
const request = require("supertest");
const app = require("../server");

const Category = require("../models/Category");

let should = chai.should();

chai.use(chaiHttp);

// pocne se sa metodom describe, u callback se doda test u it funkciji
describe("Testing of the get all categories route GET /api/categories", () => {
  it("should return status 200", done => {
    chai
      .request(app)
      .get("/api/categories")
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkM2VhZjk0NmNmOGNiMWIxNmIwOWIzMiIsIm5hbWUiOiJEdXNhQWRtaW4iLCJpYXQiOjE1NjQzODkyOTAsImV4cCI6MTU2NDQ3NTY5MH0.xTGxJkULqF9ppSKO9Jd0KlDupAOryQbI2giJNn1p1No')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("array");
        done();
      });
  });
});

describe("Testing of the creating new category route POST /api/categories", () => {
  it("should create new category, and return status 200", done => {
    request(app)
      .post("/api/categories")
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkM2VhZjk0NmNmOGNiMWIxNmIwOWIzMiIsIm5hbWUiOiJEdXNhQWRtaW4iLCJpYXQiOjE1NjQzODkyOTAsImV4cCI6MTU2NDQ3NTY5MH0.xTGxJkULqF9ppSKO9Jd0KlDupAOryQbI2giJNn1p1No')
      .send({ name: "new test category" })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property("name");
        res.body.should.have.property("name").eql("new test category");
        done();
      });
  });
});

describe("Remove testing categories", () => {
  it("should remove all categories with name new test category", done => {
    Category.deleteMany({ name: "new test category" }, function(err) {});
    done();
  });
});
