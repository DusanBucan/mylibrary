const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const BookSchema = new Schema({
  title: {
    type: String,
    required: true,
    allowNull: false
  },
  category: {
    type: Schema.Types.ObjectId,
    required: true,
    allowNull: false,
    ref: "categories"
  },
  author: {
    type: String,
    required: true,
    allowNull: false
  },
  isbn: {
    type: String,
    required: true,
    allowNull: false
  },
  publishDate: {
    type: Date,
    required: true,
    allowNull: false
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }
});

module.exports = Book = mongoose.model("books", BookSchema);
