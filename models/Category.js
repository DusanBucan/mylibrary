const mongoose = require("mongoose");
const Schema = mongoose.Schema;  // kreira se nova INSTANCA SEME

// Create Schema
const CategorySchema = new Schema({
  name: {
    type: String,
    required: true,
    allowNull: false
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }
});


// mongoose.model --> kompajlira semu i smesta je u Category
// posto smo namestili ovu semu da li se ona DODAJE NA mongoose INSTANCU???? MSM DA NE, samo da se EXPORTUJE INSTANCA SEME
        // instanca SEME CE BITI ISTA GDE GOD URADIM require naziv seme
module.exports = Category = mongoose.model("categories", CategorySchema);
