import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { CategoryService } from '../../categories/category.service';
import { Observable } from 'rxjs';
import { Category } from 'src/app/categories/category.model';

@Component({
  selector: 'library-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  categories$: Observable<Category[]>;

  // objekat emitera je ovo sa dekoratorom output --> ovo je kao event u Vue. sad imas
  @Output() categorySelected = new EventEmitter<number>(); // KORISCENO ZA 1. nacin
  selectedCategoryId: number = null;

  // ovako radi inject servisa --> DI preko konstruktora.. ti kazes da ti treba a on ce ga posle sam dodati
  constructor(private categorySevice: CategoryService) {}

  ngOnInit() {
    this.categories$ = this.categorySevice.getCategories();

    // subscribe ima 3 fije ovo sto smo mi dali je uspesna, treba call back i za error i za complete
    // this.categories$.subscribe(
    //   data => {
    //     console.log(data);
    //   },
    //   error => {
    //     console.error(error);
    //   },
    //   () => {
    //     console.log('complete');
    //   }
    // );
  }

  onCategorySelect(categoryId: number) {
    this.selectedCategoryId = categoryId;

    // ovo je bilo na prvi nacin
    //this.categorySelected.emit(categoryId);

    // drugi nacin sa servisima
    this.categorySevice.categoryFilterChanged.next(categoryId);
  }
}
