import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { Book } from 'src/app/books/book.model';
import { BookService } from 'src/app/books/book.service';
import { CategoryService } from 'src/app/categories/category.service';

@Component({
  selector: 'library-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {
  //@Input() selectedCategoryId: number;  korisceno za prvi nacin
  books$: Observable<Book[]>;

  // drugi nacin komunikacije komponenti
  selectedCategoryId: number;
  categoryFilterSubscription: Subscription;

  constructor(
    private bookService: BookService,
    private categoryService: CategoryService
  ) {}

  ngOnInit() {
    this.books$ = this.bookService.getBooks();

    this.categoryFilterSubscription = this.categoryService.categoryFilterChanged.subscribe(
      selectedCategoryId => {
        this.selectedCategoryId = selectedCategoryId;
      }
    );
  }

  // da ne bi doslo do memory leak-a ---> kad ubijas komponentu na sve sto je pretplacena TREBA DA se otkaci
  ngOnDestroy() {
    this.categoryFilterSubscription.unsubscribe();
  }
}
