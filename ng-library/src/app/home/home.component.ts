import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'library-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // koristi se u prvom nacinu kad se komunicira preko parenta
  //selectedCategoryID: number;

  // brise se props iz .html fajla koji je prosledjivao ovo na ono @Input u category-list.component.ts

  constructor() {}

  ngOnInit() {}

  /* koristi se u prvom nacinu kad se komunicira preko parenta ---> OBIRSANO JE IZ .html komponente 
    <library-category-list
      (categorySelected)="selectCategory($event)"  
    ></library-category-list>
  */


  // selectCategory(categoryId: number) {
  //   this.selectedCategoryID = categoryId;
  // }
}
