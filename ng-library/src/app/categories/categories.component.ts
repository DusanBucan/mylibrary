import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from './category.service';
import { Observable } from 'rxjs';
import { Category } from './category.model';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'library-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories$: Observable<Category[]>;
  // mozes da prosledis i objekat ovako bez konstruktora za neke jednostavne stvari
  selectedCategory: Category = { _id: null, name: null };

  // dekorator
  // dobavljas formu iz html dela kompoente preko one reference, uradi query nad html-om ,
  @ViewChild('f', { static: false }) addCategoryForm: NgForm;
  error: { name: string };

  operation: string;

  constructor(private categoryService: CategoryService) {}

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
  }

  onCategoryEdit(category: Category) {
    this.operation = 'Edit';
    this.selectedCategory = Object.assign({}, category);
    this.error = null;
  }

  onCategoryDelete(category: Category) {
    this.selectedCategory = Object.assign({}, category);
  }

  onCategoryDeleteSubmit() {
    this.categoryService.deleteCategory(this.selectedCategory._id).subscribe(
      () => {
        this.categories$ = this.categoryService.getCategories();
        this.selectedCategory = { _id: null, name: null };
      },
      error => console.error(error)
    );
  }

  onCategoryAdd() {
    this.operation = 'Add';
    this.addCategoryForm.reset();
    this.selectedCategory = { _id: null, name: null };
    this.error = null;
  }

  onCategorySaveSubmit(form: NgForm, closeButton: HTMLButtonElement) {
    this.categoryService
      .saveCategory({
        _id: this.operation === 'Add' ? null : this.selectedCategory._id,
        name: form.value.name
      })
      .subscribe(
        () => {
          this.categories$ = this.categoryService.getCategories();
          closeButton.click(); // ovako ces da zatvoris modalni dijalog, ako je sve okej
        },
        (httpErrorResponse: HttpErrorResponse) => {
          this.error = httpErrorResponse.error;
        }
      );
  }
}
