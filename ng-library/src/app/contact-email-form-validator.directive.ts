import { Directive, Input } from '@angular/core';
import {
  NG_VALIDATORS,
  Validator,
  AbstractControl,
  ValidatorFn,
  ValidationErrors
} from '@angular/forms';
import { AbstractClassPart } from '@angular/compiler/src/output/output_ast';

@Directive({
  selector: '[libraryMyValidationDirective]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ContactEmailFormValidatorDirective,
      multi: true
    }
  ]
})
export class ContactEmailFormValidatorDirective implements Validator {
  constructor() {}

  validate(control: AbstractControl): { [key: string]: any } | null {
    return control.value ? this.emailValidatorFunction(control.value) : null;
  }

  emailValidatorFunction(email: string): ValidationErrors | null {
    console.log(email);
    const isValid = email.includes('@');
    console.log('proveravam');
    return !isValid ? { identityRevealed: true } : null;
  }
}
