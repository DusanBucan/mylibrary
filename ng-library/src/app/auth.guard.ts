import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { take, map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})

/*
  next ruta na koju idemo
  state je ruta sa koje idemo

*/
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const expectedRole = next.data.expectedRole;
    return this.authService.getCurrentUser().pipe(
      map(user => {
        if (
          expectedRole === 'user' ||
          (expectedRole === 'admin' && user.role === 4)
        ) {
          return true; // da moze da ode na rutu
        }
        return this.router.createUrlTree(['/home']);
        // da izgradis URL rutu na koju ces da ga posaljes ---> moze da se iskoristi da ga vratis ako nije Auth
      }),
      catchError(() => {
        return of(this.router.createUrlTree(['/login']));
      })
    );
  }
}
