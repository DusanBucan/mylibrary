import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'library-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  error:any;
  
  constructor() {}

  ngOnInit() {}

  onContactSubmit(form: NgForm, closeButton: HTMLButtonElement) {
    console.log(
      'email: ' + form.value.email + ' message: ' + form.value.message
    );
  }

  onContactCancel(form: NgForm) {
    form.reset();
    console.log('pobrisano iz forme');
  }
}
