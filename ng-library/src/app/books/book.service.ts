import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Book } from './book.model';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

// providedIn root znaci da ce nam instanca biti kreirana za root koponentu i svaka komponenta koja je child od root ce koristiti istu
// taj servis kao u Spring-u

// observalbe je genericka klasa

// rxjs sadrzi observable

// u app.modules mozemo da stavimo u providers dodajemo nas servis ---> OVO je MORALO PRE
// mozes i da stavis u svakom @NgModules --> provides:[naziv_servisa] ---> ta kompoenta i sve komponente koje su child ove bi imale isti servis
// --> razliciti @NgModules ce imati razlicite servise injektovane

@Injectable({
  providedIn: 'root'
})
export class BookService {
  //API = "http://localhost:5000/api/books";
  API = 'api/books'; // deploy API

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getBooks(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.API, {
      headers: this.authService.getAuthHeaders()
    });
  }

  saveBook(book: Book): Observable<any> {
    return this.httpClient.post(this.API, book, {
      headers: this.authService.getAuthHeaders()
    });
  }

  deleteBook(bookId: number) {
    return this.httpClient.delete(this.API + '/' + bookId, {
      headers: this.authService.getAuthHeaders()
    });
  }
}
