import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { BooksComponent } from './books/books.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryListComponent } from './home/category-list/category-list.component';
import { BookListComponent } from './home/book-list/book-list.component';
import { FilterBooksByCategoryPipe } from './filter-books-by-category.pipe';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { ContactComponent } from './contact/contact.component';
import { ContactEmailFormValidatorDirective } from './contact-email-form-validator.directive';
import { RegistrationComponent } from './registration/registration.component';
import { AuthService } from './auth.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    BooksComponent,
    CategoriesComponent,
    CategoryListComponent,
    BookListComponent,
    FilterBooksByCategoryPipe,
    NotFoundPageComponent,
    ContactComponent,
    ContactEmailFormValidatorDirective,
    RegistrationComponent,
    LoginComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
