const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function vallidateBookInput(data) {
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : "";
  data.author = !isEmpty(data.author) ? data.author : "";
  data.publishDate = !isEmpty(data.publishDate) ? data.publishDate : "";
  data.isbn = !isEmpty(data.isbn) ? data.isbn : "";

  if (Validator.isEmpty(data.title)) {
    errors.title = "Title field is required";
  }

  if (Validator.isEmpty(data.author)) {
    errors.author = "Author field is required";
  }

  if (Validator.isEmpty(data.publishDate)) {
    errors.publishDate = "Publish date field is required";
  }

  if (Validator.isEmpty(data.isbn)) {
    errors.isbn = "Isbn field is required";
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};
